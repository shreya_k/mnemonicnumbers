#include <iostream>
#include <string>

using namespace std;

string encodeToNum(string);
string encodeLetter(string);

int main() {
    string nameToken = "vimid";
    cout << encodeToNum(nameToken) << endl;
    return 0;
}

/*Accepts a 5-letter name and returns a mnemonic phone number based on T9 standard*/
string encodeToNum(string name) {
    string phNum = "";
 
    for(int i = 0; i <= (name.size() - 1); i++) {  
        phNum = phNum + encodeLetter(name.substr(i, 1));   
    }

    return phNum;
}

/*Accepts a letter and returns its T9 number*/
string encodeLetter(string letter) {
    string alphabets = "abcdefghijklmnopqrstuvwxyz";
    string valOfLetter = "22233344455566677778889999";

    int pos = alphabets.find(letter);

    return valOfLetter.substr(pos, 1);
}

