#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

vector<string> readFile();
vector<string> mnemonicNum(vector<string>);

int main() {
    vector<string> namelist = readFile();
    vector<string> number = mnemonicNum(namelist);
    return 0;
}


vector<string> readFile() {
    string name;
    vector<string> namelist;
    vector<string> number;
    ifstream myfile;
    myfile.open("names.list");

    while(!myfile.eof()) {
        getline(myfile, name);
        namelist.push_back(name);
//        parseName(name);
//      numer.push_back(encodeToNum(name));
        cout << name;
    }
    return namelist;
}

vector<string> mnemonicNum(vector<string> namelist) {
    vector<string> number;
    vector<string> tokens = parseName(namelist);
    vector<string>::iterator iter;
    for(iter = tokens.begin(); iter!= tokens.end();iter++) {
        number.push_back(encodeToNum(iter));
    }
    return number;
}
