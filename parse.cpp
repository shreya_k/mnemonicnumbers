#include <iostream>
#include <ctype.h>
#include <vector>

using namespace std;

string parseName(string name);
string firstName(string name);
string lastName(string name);

int main(int argc, char *argv[]) {
    string name;
    getline(cin, name);
    cout << parseName(name);
    return 0;
}


string parseName(string name) {
    string id = "";
    string first = firstName(name);
    string last = lastName(name);
    if (strcmp(&first[0], &last[0]) == 0)
        first = "";
    for (int i = 0; (i < 2 && first[i] != '\0'); i++)
        id += tolower(first[i]);
    for (int i = 0; (i < 3 && last[i] != '\0'); i++)
        id += tolower(last[i]);
    while (strlen(&id[0]) < 5) {
        id += '0';
    }
    return id;
}

string firstName(string name) {
    return strtok(&name[0], " ");
}

string lastName(string name) {
    char *tmp = strtok(&name[0], " ");
    char *last = tmp;
    while (tmp != NULL) {
        last = tmp;
        tmp = strtok(NULL, " ");
    }
    return last;
}

